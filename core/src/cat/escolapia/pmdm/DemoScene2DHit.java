package cat.escolapia.pmdm;

	import com.badlogic.gdx.ApplicationListener;
	import com.badlogic.gdx.Gdx;
	import com.badlogic.gdx.graphics.Color;
	import com.badlogic.gdx.graphics.GL20;
	import com.badlogic.gdx.graphics.Texture;
	import com.badlogic.gdx.graphics.g2d.Batch;
	import com.badlogic.gdx.graphics.g2d.TextureRegion;
	import com.badlogic.gdx.scenes.scene2d.Actor;
	import com.badlogic.gdx.scenes.scene2d.InputEvent;
	import com.badlogic.gdx.scenes.scene2d.InputListener;
	import com.badlogic.gdx.scenes.scene2d.Stage;
	import com.badlogic.gdx.scenes.scene2d.Touchable;
	import com.badlogic.gdx.utils.viewport.ExtendViewport;

	import java.util.Random;

	public class DemoScene2DHit implements ApplicationListener {

		// L'Actor "Pilota" té la textura que se li passi
		class Pilota extends Actor {
			private TextureRegion _textura;
			int num_touches = 0;
			public Pilota(TextureRegion textura){
				_textura = textura;
				setBounds(getX(), getY(), _textura.getRegionWidth(), _textura.getRegionHeight());

				this.addListener(new InputListener() {
					public boolean touchDown(InputEvent event, float x, float y, int pointer, int buttons) {
						System.out.println("TouchDown_" + getName() + ": " + x + "/" + y);
						num_touches++;
						if(num_touches >= 3){
							setVisible(false);
						}
						return true;
					}
				});
			}

			// Lògica del draw
			public void draw(Batch batch, float alpha){
				Color colorActual = batch.getColor();
				if(num_touches == 2){batch.setColor(Color.RED);}
				batch.draw(_textura, this.getX(), getY());
				batch.setColor(colorActual);

				/*
				// Draw per suportar roatcions i escalat
				batch.draw(_texture, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation());
				*/
			}

			// Aquest hit() miraria un quadrat -10 píxels
			public Actor hit(float x, float y, boolean touchable) {
                if (touchable && getTouchable() != Touchable.enabled) return null;
				double ModClick = Math.sqrt((Math.pow(x-getWidth()/2,2))+(Math.pow(y-getHeight()/2,2)));

                if (ModClick <= getWidth()/2) {
                    return this;
                } else  return  null;
            }
			/*
			 if (x >= 10 && x < getWidth() - 10 && y >= 10 && y < getHeight() -10 ) {
					System.out.println(getWidth() - 10);
					System.out.println(getHeight() - 10);
                    return this;
                }

			 */
		}

		private Pilota[] pilotes;
		private Stage stage;

		@Override
		public void create() {
			stage = new Stage(new ExtendViewport( Gdx.graphics.getWidth(), Gdx.graphics.getHeight()) );

			Random random = new Random();

			final TextureRegion pilotaTexture = new TextureRegion(new Texture("pilota.png"));
			pilotes = new Pilota[6];
			for(int i = 0; i < 6; i++){
				pilotes[i] = new Pilota(pilotaTexture);
				//Posicionem l'actor aletatòriament, li posem com a nom el numeroal i l'afegim a l'Stage
				pilotes[i].setPosition(random.nextInt(Gdx.graphics.getWidth() - (int) pilotes[i].getWidth()), random.nextInt(Gdx.graphics.getHeight() - (int) pilotes[i].getHeight()));
				pilotes[i].setName(Integer.toString(i));
				stage.addActor(pilotes[i]);
			}

			Gdx.input.setInputProcessor(stage);
		}

		@Override
		public void dispose() {
			stage.dispose();
		}

		@Override
		public void render() {
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			stage.act(Gdx.graphics.getDeltaTime());
			stage.draw();
		}

		@Override
		public void resize(int width, int height) {
            stage.getViewport().update(width, height, true);
		}

		@Override
		public void pause() {
		}

		@Override
		public void resume() {
		}
	}